angular.module('starter', ['ionic', 'ngCordova'])

.run(function($ionicPlatform) {

    var defaultOptions = {
        location: 'no',
        clearcache: 'no',
        toolbar: 'no'
    };

    $ionicPlatform.ready(function() {

        if (window.cordova && window.cordova.InAppBrowser) {
            // $cordovaInAppBrowserProvider.setDefaultOptions(options);
            window.open = window.cordova.InAppBrowser.open;

            console.info('InAppBrowser Activado');
        } else {
            console.info('InAppBrowser NO Activado');
        }

        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

            cordova.plugins.Keyboard.disableScroll(true);
        }
        if (window.StatusBar) {
            StatusBar.styleDefault();
        }
    });
})

.controller('MainCtrl', function($ionicPlatform, $cordovaInAppBrowser, $rootScope) {
    var main = this;
    
    var options = {
        location: 'no',
        clearcache: 'yes',
        toolbar: 'yes'
    };
    
    $ionicPlatform.ready(function() {
        // window.open('Gps.controlve.com/mobile', '_system', 'location=yes');
        // window.open('http://Gps.controlve.com/mobile', '_self', options);
        $cordovaInAppBrowser
            .open('http://Gps.controlve.com/mobile', '_self', options)
            .then(function(event) {
                // success
            })
            .catch(function(event) {
                // error
            });
    });

    $rootScope.$on('$cordovaInAppBrowser:exit', function(e, event) {
        console.info('Preparing to exit the app');
        ionic.Platform.exitApp();
    });

})

.controller('ThisCtrl', function($cordovaInAppBrowser) {

    var options = {
        location: 'yes',
        clearcache: 'yes',
        toolbar: 'no'
    };

    document.addEventListener("deviceready", function() {
        $cordovaInAppBrowser.open('http://ngcordova.com', '_blank', options)
            .then(function(event) {
                // success
            })
            .catch(function(event) {
                // error
            });


        $cordovaInAppBrowser.close();

    }, false);

    $rootScope.$on('$cordovaInAppBrowser:loadstart', function(e, event) {

    });

    $rootScope.$on('$cordovaInAppBrowser:loadstop', function(e, event) {
        // insert CSS via code / file
        $cordovaInAppBrowser.insertCSS({
            code: 'body {background-color:blue;}'
        });

        // insert Javascript via code / file
        $cordovaInAppBrowser.executeScript({
            file: 'script.js'
        });
    });

    $rootScope.$on('$cordovaInAppBrowser:loaderror', function(e, event) {

    });

    $rootScope.$on('$cordovaInAppBrowser:exit', function(e, event) {

    });

})

;